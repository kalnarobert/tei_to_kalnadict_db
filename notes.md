

sqlite query to ask for dictionary tabls:

```sql
select name from sqlite_master where type = 'table' and name like '%_dictionary';
```

sample query to create index:

```sql
CREATE INDEX `en_accent_index` ON `en_dictionary` (`id` ASC,`langid` ,`word` ASC)
CREATE INDEX `en_no_accent_index` ON `en_dictionary` (`no_accent_id` ASC,`langid` ,`word_without_accents` ASC)
```

# css

update hu_en_meta set value='
a {
  text-decoration: none;color:black;
  pointer-events: none;
  cursor: default;
}
a:link {
  text-decoration: none;color:black;background-color:#eeeeee;
}
a:visited {
  text-decoration: none;color:black;background-color:#eeeeee
}
a:active {
  text-decoration: none;color:black;background-color:#eeeeee
}
a:hover {
  text-decoration: none;
  color:black;
  background-color: rgba(158, 154, 154, 0.65)
}

p {
  font-family : Times New Roman;
  font-weight : normal;
  font-style : normal;
  font-size : 70%;
  vertical-align : 40%;
}

div {
  text-align : left !important;
}

p {
  color: red;
}

body {
  /* Set "my-sec-counter" to 0 */
  counter-reset: my-sec-counter;
}

ul.sense {
    padding-left: 10px;
}

ul::before {
  /* Increment "my-sec-counter" by 1 */
  counter-increment: my-sec-counter;
  content: counter(my-sec-counter) ".";
}

li {
  display: inline-block;
}

li:after {
  content: ", ";
}

li:last-child:after {
  content: "";
}

' where key like '%css';
