
# About

This project contains scripts to create SQLite databases from a tei format dictionary, especially
for the open dictionary project [here][freedict]

[freedict]: https://github.com/freedict/fd-dictionaries

# Install 

Set up a python virtualenv as described here: https://gist.github.com/bradmontgomery/d1423d8e94e29e45aed3 

```
python3 -m venv create_db_from_tei_env
source create_db_from_tei_env/bin/activate
```

and install requirements:

```
pip install -r requirements.txt
```

# Usage


