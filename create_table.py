#! /usr/bin/python3

import jinja2
from lxml import etree
import unidecode
import sqlite3
from sqlite3 import Error
import sys
import re
import os.path
import progressbar

import argparse

parser = argparse.ArgumentParser(description='create sqlite database from tei file')
parser.add_argument('file', metavar='file.tei', help='tei file to be processed')
parser.add_argument('--db', dest='database', help='use given database file')
parser.add_argument('--langid', dest='langid', help='default langid=1, change if needed (originally 1 or 2)')
parser.add_argument('lang', help='language for which the table is created')
parser.add_argument('--lang2', help='secondary language for which the table is created')
parser.add_argument('--css1', help='file containing css for primary language translation views')
parser.add_argument('--css2', help='file containing css for secondary language translation views')

args = parser.parse_args()

if not os.path.exists(args.file):
    print("Error -", args.file, "does not exist")
    sys.exit()

if args.langid is not None:
    LANG_ID = args.langid
else:
    LANG_ID = 1
if args.database is not None:
    db = args.database
else:
    db = args.lang + "_dict.db"

tree = etree.parse(args.file)
stringify = etree.XPath("string()")
namespace_map = {'ns': tree.getroot().nsmap[None]}
templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "template"
template = templateEnv.get_template(TEMPLATE_FILE)
create_dic_table = templateEnv.get_template("create_table.template")
create_fav_table = templateEnv.get_template("create_favorites_table.template")
create_ind_table = templateEnv.get_template("create_index_table.template")
create_met_table = templateEnv.get_template("create_meta_table.template")
fill_ind_table = templateEnv.get_template("fill_index_table.template")
fill_met_table = templateEnv.get_template("fill_meta_table.template")


def load_css(css1, css2):
    (ret1, ret2) = ("", "")
    if os.path.exists(css1):
        with open(css1, 'r') as file:
            ret1 = file.read()
    if os.path.exists(css2):
        with open(css2, 'r') as file:
            ret2 = file.read()
    return ret1, ret2


try:
    conn = sqlite3.connect(db)
    lang = args.lang
    if args.lang2 is None:
        dict_id = "{}".format(lang)
    else:
        dict_id = "{}_{}".format(lang, args.lang2)

    table_name = "{}_dictionary".format(dict_id)
    fav_table_name = "{}_favorites".format(dict_id)
    ind_table_name = "{}_index".format(dict_id)
    meta_table_name = "{}_meta".format(dict_id)
    # conn.execute("drop table if exists {}".format(table_name))
    # conn.execute("drop table if exists {}".format(fav_table_name))
    # conn.execute("drop table if exists {}".format(ind_table_name))
    conn.execute(create_dic_table.render(name=table_name))
    conn.execute(create_fav_table.render(name=fav_table_name))
    conn.execute(create_met_table.render(name=meta_table_name))
    (css1, css2) = load_css(args.css1, args.css2)
    fill_meta_sql = fill_met_table.render(meta=meta_table_name, lang=lang, lang2=args.lang2, css1=css1, css2=css2);
    conn.executescript(fill_meta_sql)
    cur = conn.cursor()
    entries = tree.xpath("//ns:entry", namespaces=namespace_map)
    cur.execute("select count(*) from {} where langid = {}".format(table_name, LANG_ID))
    record_count = cur.fetchone()[0]
    if record_count < len(entries):
        print("Inserting ", len(entries), "entries...")
        bar = progressbar.ProgressBar(max_value=len(entries))
        i = 0
        for entry in entries:
            i = i + 1
            bar.update(i)
            form = stringify(entry.xpath("ns:form/ns:orth", namespaces=namespace_map)[0])
            pronounciations = entry.xpath("ns:form/ns:pron", namespaces=namespace_map)
            gram_grp = entry.xpath("ns:gramGrp", namespaces=namespace_map)
            dict = {}
            dict["form"] = form.strip()
            if len(pronounciations) > 0:
                dict["pron"] = stringify(entry.xpath("ns:form/ns:pron", namespaces=namespace_map)[0]).strip()
            if len(gram_grp) > 0:
                dict["gram"] = ','.join(
                    [stringify(elem).strip() for elem in gram_grp[0].xpath("*", namespaces=namespace_map)])
            dict["senses"] = []
            for sense in entry.xpath("ns:sense", namespaces=namespace_map):
                dict["senses"] = dict["senses"] + [
                    [str(stringify(cit)).strip() for cit in sense.xpath("ns:cit", namespaces=namespace_map)]]
            html = template.render(dict)
            conn.execute("insert into " + table_name +
                         "(langid,word,word_without_accents,translation) values(?,?,?,?)",
                         (LANG_ID, form, re.sub("[-.']", '', unidecode.unidecode(form)), html))
            conn.commit()
    else:
        print("entries already inserted. proceeding to recounting of accent indepenet entries...")
    cur.execute("select count(no_accent_id) from {} where langid = {}".format(table_name, LANG_ID))
    record_count = cur.fetchone()[0]
    if record_count < len(entries):
        bar = progressbar.ProgressBar(max_value=len(entries))
        counter = 1
        print("Indexing by accent independent words...")
        for row in conn.execute(
                "select id from {} where langid={} order by word_without_accents".format(table_name, LANG_ID)):
            conn.execute("update {} set no_accent_id={} where id={}".format(table_name, counter, row[0]))
            bar.update(counter)
            counter = counter + 1
    else:
        print("entries already indexed by accent independent word forms. proceeding to creating index tables...")
    conn.commit()

    print("\nCreating index tables...")
    conn.executescript(create_ind_table.render(name=dict_id))
    conn.executescript(fill_ind_table.render(index=ind_table_name, dict=table_name, langid=LANG_ID))
    for row in conn.execute(
            "select Letters from {tbl} where no_accent_id=0 and lang_id={langid}".format(tbl=ind_table_name,
                                                                                         langid=LANG_ID)):
        cur.execute("select max(id), substr(word,1,2) from {tbl} where substr(word,1,2)==? and langid={langid}".format(
            tbl=table_name, langid=LANG_ID), (row[0],))
        max_id = cur.fetchone()[0]
        cur.execute("select min(id), substr(word,1,2) from {tbl} where substr(word,1,2)==? and langid={langid}".format(
            tbl=table_name, langid=LANG_ID), (row[0],))
        min_id = cur.fetchone()[0]
        conn.execute(
            "update {tbl} set fromId={from_id}, toId={to} where Letters=? and no_accent_id=0 and lang_id={langid}".format(
                tbl=ind_table_name, from_id=min_id, to=max_id, langid=LANG_ID), (row[0],))
    conn.commit()

    for row in conn.execute(
            "select Letters from {tbl} where no_accent_id=1 and lang_id={langid}".format(tbl=ind_table_name,
                                                                                         langid=LANG_ID)):
        cur.execute(
            "select max(no_accent_id), substr(word_without_accents,1,2) from {tbl} where substr(word_without_accents,1,2)==? and langid={langid}".format(
                tbl=table_name, langid=LANG_ID), (row[0],))
        max_id = cur.fetchone()[0]
        cur.execute(
            "select min(no_accent_id), substr(word_without_accents,1,2) from {tbl} where substr(word_without_accents,1,2)==? and langid={langid}".format(
                tbl=table_name, langid=LANG_ID), (row[0],))
        min_id = cur.fetchone()[0]
        conn.execute(
            "update {tbl} set fromId={from_id}, toId={to} where Letters=? and no_accent_id=1 and lang_id={langid}".format(
                tbl=ind_table_name, from_id=min_id, to=max_id, langid=LANG_ID), (row[0],))
    conn.commit()

except Error as e:
    print(e)
finally:
    conn.close()
